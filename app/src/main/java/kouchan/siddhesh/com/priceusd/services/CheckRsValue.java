package kouchan.siddhesh.com.priceusd.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import kouchan.siddhesh.com.priceusd.AppController;
import kouchan.siddhesh.com.priceusd.Model.OttoDisplayPriceModel;
import kouchan.siddhesh.com.priceusd.Otto.EventBusManager;
import kouchan.siddhesh.com.priceusd.Utils.NotificationOperator;
import kouchan.siddhesh.com.priceusd.Utils.PrefManager;
import kouchan.siddhesh.com.priceusd.view.MainActivity;

import static android.content.ContentValues.TAG;

public class CheckRsValue extends Service {

    public static final String  checkPriceUrl="https://api.coinmarketcap.com/v1/ticker/ripple";
    PrefManager prefManager;
    NotificationOperator notificationOperator;
    Double rs;

    public CheckRsValue() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {


    prefManager= new PrefManager(getApplicationContext());
        notificationOperator=new NotificationOperator(getApplicationContext());

        (new Thread(new Runnable()
        {

            @Override
            public void run()
            {
                while (!Thread.interrupted())
                    try
                    {
                        Thread.sleep(5000);

                        checkRsPrice();

                    }
                    catch (InterruptedException e)
                    {

                    }
            }
        })).start();


    }
    private void checkRsPrice() {

        JsonArrayRequest req = new JsonArrayRequest(checkPriceUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        try {

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject person = (JSONObject) response.get(i);
                                String price_usd = person.getString("price_usd");
                                Double usd= Double.parseDouble(price_usd);
                                rs= usd*65.0;

                            }
                            prefManager.setCurrentPrice(Double.toString(rs));

                            if((prefManager.getPreviousPrice()).equals("notSet")){

                                prefManager.setPreviousPrice(Double.toString(rs));
                            }else {

                                Double currentPrice=Double.parseDouble(prefManager.getCurrentPrice());
                                Double previousPrice=Double.parseDouble(prefManager.getPreviousPrice());

                                if (Math.abs(currentPrice-previousPrice)>=0.1){

                                    Intent intent1= new Intent(getApplicationContext(),MainActivity.class);
                                    notificationOperator.showNotification("Price changed","Price change by "+Double.toString(currentPrice-previousPrice) , intent1);
                                }

                                prefManager.setPreviousPrice(Double.toString(rs));

                            }

                            OttoDisplayPriceModel ottoEventActivityFinish = new OttoDisplayPriceModel(Double.toString(rs));
                            EventBusManager.getInstance().getEventBus().post(ottoEventActivityFinish);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
               /* Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();*/
            }
        });


        AppController.getInstance().addToRequestQueue(req);
    }
    }