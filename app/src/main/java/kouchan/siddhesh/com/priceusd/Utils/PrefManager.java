package kouchan.siddhesh.com.priceusd.Utils;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String CURRENT_PRICE = "currentPrice";
    private static final String OLD_PRICE = "oldprice";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setCurrentPrice(String current_price) {
        editor.putString(CURRENT_PRICE, current_price);
        editor.commit();
    }

    public String getCurrentPrice() {
        return pref.getString(CURRENT_PRICE, null);
    }


    public void setPreviousPrice(String current_price) {
        editor.putString(OLD_PRICE, current_price);
        editor.commit();
    }

    public String getPreviousPrice() {
        return pref.getString(OLD_PRICE,"notSet");
    }
}