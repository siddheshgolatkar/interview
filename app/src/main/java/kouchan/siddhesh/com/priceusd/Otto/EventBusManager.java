package kouchan.siddhesh.com.priceusd.Otto;

import com.squareup.otto.Bus;

/**
 * Created by basavaraj on 4/20/17.
 */

public class EventBusManager {
  private static EventBusManager ourInstance = new EventBusManager();
  private Bus cloveBus = new EventBus();

  private EventBusManager() {

  }

  public static EventBusManager getInstance() {
    return ourInstance;
  }

  public Bus getEventBus() {
    return cloveBus;
  }

}
