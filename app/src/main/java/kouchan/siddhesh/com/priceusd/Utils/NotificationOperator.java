package kouchan.siddhesh.com.priceusd.Utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import kouchan.siddhesh.com.priceusd.R;


public class NotificationOperator
{
    Context notificatinContext;
    public static final int ID_NOTIFICATION=222;

    public NotificationOperator(Context context)
    {
        notificatinContext=context;
    }

    public void showNotification(String title, String message, Intent intent)
    {
        PendingIntent resultPendingResult= PendingIntent.getActivity(notificatinContext,ID_NOTIFICATION,intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder=new NotificationCompat.Builder(notificatinContext);

        Notification notification;
        notification = builder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingResult)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setDefaults(Notification.DEFAULT_ALL)
                .setLargeIcon(BitmapFactory.decodeResource(notificatinContext.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .setPriority(Notification.PRIORITY_MAX)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) notificatinContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_NOTIFICATION, notification);
    }
}
