package kouchan.siddhesh.com.priceusd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import kouchan.siddhesh.com.priceusd.services.CheckRsValue;

/**
 * Created by KOUCHAN-ADMIN on 03-Apr-18.
 */

public class MyBroadcastreceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, CheckRsValue.class);
        context.startService(startServiceIntent);
    }
}