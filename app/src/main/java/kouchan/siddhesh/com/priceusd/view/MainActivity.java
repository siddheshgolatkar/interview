package kouchan.siddhesh.com.priceusd.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import kouchan.siddhesh.com.priceusd.Model.OttoDisplayPriceModel;
import kouchan.siddhesh.com.priceusd.Otto.EventBusManager;
import kouchan.siddhesh.com.priceusd.R;
import kouchan.siddhesh.com.priceusd.Utils.NotificationOperator;
import kouchan.siddhesh.com.priceusd.Utils.PrefManager;
import kouchan.siddhesh.com.priceusd.services.CheckRsValue;

public class MainActivity extends AppCompatActivity  {

    String  checkPriceUrl="https://api.coinmarketcap.com/v1/ticker/ripple";

    TextView price;
    PrefManager prefManager;
    NotificationOperator notificationOperator;
    Double rs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        price=(TextView) findViewById(R.id.textView3);
        prefManager= new PrefManager(this);
        notificationOperator=new NotificationOperator(getApplicationContext());
        EventBusManager.getInstance().getEventBus().register(this);

       /* checkRsPrice();*/

        Intent i= new Intent(this,CheckRsValue.class);
        startService(i);



    }

/*    private void checkRsPrice() {
        JsonArrayRequest req = new JsonArrayRequest(checkPriceUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {
                            // Parsing json array response
                            // loop through each json object
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject person = (JSONObject) response.get(i);
                                String price_usd = person.getString("price_usd");
                                Double usd= Double.parseDouble(price_usd);
                                rs= usd*65;

                            }
                            prefManager.setCurrentPrice( Double.toString(rs));

                            price.setText(Double.toString(rs));

                            if((prefManager.getPreviousPrice()).equals("notSet")){

                                prefManager.setPreviousPrice(Double.toString(rs));
                            }else {

                                Double currentPrice=Double.parseDouble(prefManager.getCurrentPrice());
                                Double previousPrice=Double.parseDouble(prefManager.getPreviousPrice());

                                if (Math.abs(currentPrice-previousPrice)>=0.1){

                                    Intent intent1= new Intent(getApplicationContext(),MainActivity.class);
                                    notificationOperator.showNotification("Price changed","Price change by "+Double.toString(currentPrice-previousPrice) , intent1);

                                }
                                prefManager.setPreviousPrice(Double.toString(rs));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                     Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }


                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        AppController.getInstance().addToRequestQueue(req);
    }*/

@Subscribe
    public void setPriceFromService(OttoDisplayPriceModel ottoDisplayPriceModel){

        price.setText(ottoDisplayPriceModel.getPrice());

}




}
